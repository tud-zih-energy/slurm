#ifndef _EXT_SENSORS_METRICQ_H_
#define _EXT_SENSORS_METRICQ_H_

extern "C"
{
#include <src/slurmctld/slurmctld.h>

typedef struct ext_sensors_config {
  char *metricq_server;
  char *metricq_token;
  char *metricq_metric;
} ext_sensors_conf_t;

extern int ext_sensors_read_conf(void);
extern void ext_sensors_free_conf(void);
extern int ext_sensors_p_update_component_data(void);
extern int ext_sensors_p_get_stepstartdata(step_record_t *step_rec);
extern int ext_sensors_p_get_stependdata(step_record_t *step_rec);
extern List ext_sensors_p_get_config(void);
}
#endif
