/*****************************************************************************\
 *  ext_sensors_none.c - slurm external sensors plugin for none.
 *****************************************************************************
 *  Copyright (C) 2013
 *  Written by Bull- Thomas Cadeau/Martin Perry/Yiannis Georgiou
  *
 *  This file is part of Slurm, a resource management program.
 *  For details, see <https://slurm.schedmd.com/>.
 *  Please also read the included file: DISCLAIMER.
 *
 *  Slurm is free software; you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free
 *  Software Foundation; either version 2 of the License, or (at your option)
 *  any later version.
 *
 *  In addition, as a special exception, the copyright holders give permission
 *  to link the code of portions of this program with the OpenSSL library under
 *  certain conditions as described in each individual source file, and
 *  distribute linked combinations including the two. You must obey the GNU
 *  General Public License in all respects for all of the code used other than
 *  OpenSSL. If you modify file(s) with this exception, you may extend this
 *  exception to your version of the file(s), but you are not obligated to do
 *  so. If you do not wish to do so, delete this exception statement from your
 *  version.  If you delete this exception statement from all source files in
 *  the program, then also delete it here.
 *
 *  Slurm is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *  FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with Slurm; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA.
 *
 \*****************************************************************************/
extern "C"
{
#include "src/common/slurm_xlator.h"
#include <errno.h>
#include <fcntl.h>
#include <inttypes.h>
#include <math.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/un.h>
#include <unistd.h>
#include "ext_sensors_metricq.h"

#include "src/interfaces/jobacct_gather.h"
#include "src/common/read_config.h"
#include "src/common/xstring.h"
}

#include <metricq/history_client.hpp>
#include <metricq/types.hpp>
#include <memory>
#include <chrono>

class SimpleHistoryClient : public metricq::HistoryClient
{
public:
	  SimpleHistoryClient(std::string manager_host, std::string token, std::chrono::system_clock::time_point begin, std::chrono::system_clock::time_point end, std::vector<std::string> hosts)
    : HistoryClient(token, true), begin_(begin), end_(end), hosts_(hosts)
  {
    connect(manager_host);
  }
  

  double value()
  {
	  return value_;
  }
protected:

  metricq::json handle_discover_rpc(const metricq::json& json) override {return json;}
  void on_history_response(const std::string &id,
                           const metricq::HistoryResponseAggregateView &view) {
	warning("GOT DATA\n");
	  for (auto tva  : view)
	{
		value_ += (tva.max - tva.min) * 3600;
	}
	  stop();
  }
  void on_history_response(const std::string &id,
                           const metricq::HistoryResponseValueView &view) {
  }
  void on_history_response(const std::string &id, const std::string &error) {
    warning("on_error hits the fan: %s",  error.c_str());
  }
  void on_error(const std::string& message)
{
    warning("on_error hits the fan: %s",  message.c_str());
	  stop();
}

void on_closed()
{
    warning("DummyHistory::on_closed() called");
	  stop();
}

  void on_history_config(const metricq::json &config) {}
  void on_history_ready() {
	warning("CONFIGURING HOST\n");
	  std::map<std::string, std::string> metrics = { { "spr1.gh.dkrz.de", "pdu.ActiveEnergy.SPR1" }, 
		  {"spr2.gh.dkrz.de", "pdu.ActiveEnergy.SPR2"},
		  { "genoa.gh.dkrz.de", "pdu.ActiveEnergy.Genoa"},
		  {"aurora.gh.dkrz.de", "pdu.snmp.pdu2.ActiveEnergy.Aurora1"},
		  {"gracehopper.gh.dkrz.de", "pdu.ActiveEnergy.GraceHopper"},
		  {"gracegrace.gh.dkrz.de", "pdu.ActiveEnergy.GraceGrace"}};
	  for(const auto& host : hosts_)
	  {
		  warning("%s", host.c_str());
		  if(metrics.count(host))
		  {
    			history_request(metrics[host], begin_, end_, end_ - begin_, Type::AGGREGATE);

		  }
		  else
		  {
			  warning("Did not find host: %s", host.c_str());
		  }
	  }
 }
private:
  std::vector<std::string> hosts_;
  metricq::TimePoint begin_, end_;
  double value_ = 0;
};

	/*
 * These variables are required by the generic plugin interface.  If they
 * are not found in the plugin, the plugin loader will ignore it.
 *
 * plugin_name - a string giving a human-readable description of the
 * plugin.  There is no maximum length, but the symbol must refer to
 * a valid string.
 *
 * plugin_type - a string suggesting the type of the plugin or its
 * applicability to a particular form of data or method of data handling.
 * If the low-level plugin API is used, the contents of this string are
 * unimportant and may be anything.  Slurm uses the higher-level plugin
 * interface which requires this string to be of the form
 *
 *	<application>/<method>
 *
 * where <application> is a description of the intended application of
 * the plugin (e.g., "jobacct" for Slurm job completion logging) and <method>
 * is a description of how this plugin satisfies that application.  Slurm will
 * only load job completion logging plugins if the plugin_type string has a
 * prefix of "jobacct/".
 *
 * plugin_version - an unsigned 32-bit integer containing the Slurm version
 * (major.minor.micro combined into a single number).
 */
extern "C" {
extern const  char plugin_name[] = "ExtSensors MetricQ plugin";
extern const char plugin_type[] = "ext_sensors/metricq";
extern const uint32_t plugin_version = SLURM_VERSION_NUMBER;

static ext_sensors_conf_t ext_sensors_conf;

extern int  ext_sensors_read_conf(void)
{
  s_p_options_t options[] = {{"Server", S_P_STRING},
                             {"Token", S_P_STRING},
                             {"Metric", S_P_STRING},
                             {NULL}};
  s_p_hashtbl_t *tbl = NULL;
  char *conf_path = get_extra_conf_path("ext_sensors.conf");
  struct stat buf;
  if ((conf_path == NULL) || (stat(conf_path, &buf) == -1)) {
    fatal("ext_sensors: No ext_sensors file (%s)", conf_path);
  }

  tbl = s_p_hashtbl_create(options);

  if (s_p_parse_file(tbl, NULL, conf_path, false, NULL, false) ==
        SLURM_ERROR) {
      fatal("ext_sensors: Could not open/read/parse "
            "ext_sensors file %s",
            conf_path);
    }
  s_p_get_string(&ext_sensors_conf.metricq_server, "Server", tbl);
  s_p_get_string(&ext_sensors_conf.metricq_token, "Token", tbl);
  s_p_get_string(&ext_sensors_conf.metricq_metric, "Metric", tbl);

  s_p_hashtbl_destroy(tbl);

  xfree(conf_path);

	return SLURM_SUCCESS;
}

extern List ext_sensors_p_get_config(void)
{

  List ext_list = list_create(destroy_config_key_pair);

  config_key_pair_t *key_pair = (config_key_pair_t*)xmalloc(sizeof(config_key_pair_t));
  key_pair->name = xstrdup("Server");
  key_pair->value = xstrdup(ext_sensors_conf.metricq_server);
  list_append(ext_list, key_pair);

  key_pair = (config_key_pair_t*)xmalloc(sizeof(config_key_pair_t));
  key_pair->name = xstrdup("Token");
  key_pair->value = xstrdup(ext_sensors_conf.metricq_token);
  list_append(ext_list, key_pair);

  key_pair = (config_key_pair_t*)xmalloc(sizeof(config_key_pair_t));
  key_pair->name = xstrdup("Metric");
  key_pair->value = xstrdup(ext_sensors_conf.metricq_metric);
  list_append(ext_list, key_pair);

  return NULL;
}


extern void ext_sensors_free_conf(void)
{
  xfree(ext_sensors_conf.metricq_metric);
  xfree(ext_sensors_conf.metricq_server);
  xfree(ext_sensors_conf.metricq_token);
}

extern int ext_sensors_p_update_component_data(void)
{
	return SLURM_SUCCESS;
}

extern int ext_sensors_p_get_stepstartdata(step_record_t *step_rec)
{
	return SLURM_SUCCESS;
}

extern int ext_sensors_p_get_stependdata(step_record_t *step_rec)
{
	try 
	{

	std::chrono::time_point start_time = std::chrono::system_clock::from_time_t(step_rec->start_time);

	std::vector<std::string> hosts;
	
	hostlist_t hl = bitmap2hostlist(step_rec->step_node_bitmap);
	
	char *node_name = NULL;
	while((node_name = hostlist_shift(hl)))
	{
		hosts.emplace_back(node_name);
	}


	SimpleHistoryClient client_(std::string(ext_sensors_conf.metricq_server), std::string(ext_sensors_conf.metricq_token), start_time, metricq::Clock::now(), hosts);
	warning("STARTED MAIN_LOOP");
	client_.main_loop();
	warning("FINISHED MAIN_LOOP");
	double value = client_.value();

		step_rec->ext_sensors->consumed_energy = value;

		if(step_rec->jobacct)
		{
			step_rec->jobacct->energy.consumed_energy = value;
		}
	
	}
	catch (std::exception &e)
	{
		// Much error handling such wow, but we don't want the slurmctld
		// to keel over just because some puny plugin crapped itself
		warning("Failed to get energy data: %s\n", e.what());
	}

	return SLURM_SUCCESS;
}

/*
 * init() is called when the plugin is loaded, before any other functions
 * are called.  Put global initialization here.
 */
extern int init(void)
{
	return  ext_sensors_read_conf();
}

extern int fini(void)
{
	return SLURM_SUCCESS;
}
}
