##*****************************************************************************
#  AUTHOR:
#    Written by Bull- Thomas Cadeau
#
#  SYNOPSIS:
#    X_AC_METRICQ
#
#  DESCRIPTION:
#    Determine if the METRICQ libraries exists
##*****************************************************************************

AC_DEFUN([X_AC_METRICQ],
[
  _x_ac_metricq_dirs="/usr /usr/local"
  _x_ac_metricq_libs="lib64 lib"
  AC_LANG_PUSH([C++])
  AC_ARG_WITH(
    [metricq],
    AS_HELP_STRING(--with-metricq=PATH,Specify path to metricq-devel installation),
    [AS_IF([test "x$with_metricq" != xno && test "x$with_metricq" != xyes],
           [_x_ac_metricq_dirs="$with_metricq"])])

  if [test "x$with_metricq" = xno]; then
    AC_MSG_WARN([support for metricq disabled])
  else
    AC_CACHE_CHECK(
      [for metricq installation],
      [x_ac_cv_metricq_dir],
      [
        for d in $_x_ac_metricq_dirs; do
          test -d "$d" || continue
          test -d "$d/include" || continue
          test -d "$d/include/metricq" || continue
          test -f "$d/include/metricq/history_client.hpp" || continue
          for bit in $_x_ac_metricq_libs; do
            test -d "$d/$bit" || continue
            _x_ac_metricq_cxxflags_save="$CXXFLAGS"
            CXXFLAGS="-I$d/include $CXXFLAGS"
            _x_ac_metricq_libs_save="$LIBS"
            LIBS="-L$d/$bit -lmetricq-history $LIBS"
            AC_LINK_IFELSE(
              [AC_LANG_PROGRAM(
                 [[
                   #include <metricq/history_client.hpp>
                 ]],
                 [[
			auto begin = metricq::Clock::now();
			
                 ]],
               )],
              [AS_VAR_SET(x_ac_cv_metricq_dir, $d)],
              [])
            CXXFLAGS="$_x_ac_metricq_cxxflags_save"
            LIBS="$_x_ac_metricq_libs_save"
            test -n "$x_ac_cv_metricq_dir" && break
          done
          test -n "$x_ac_cv_metricq_dir" && break
        done
      ])

    # echo x_ac_cv_metricq_dir $x_ac_cv_metricq_dir
    if test -z "$x_ac_cv_metricq_dir"; then
      if test -z "$with_metricq"; then
        AC_MSG_WARN([unable to locate metricq installation])
      else
        AC_MSG_ERROR([unable to locate metricq installation])
      fi
    else
      METRICQ_CXXFLAGS="-I$x_ac_cv_metricq_dir/include"
      if test "$ac_with_rpath" = "yes"; then
        METRICQ_LDFLAGS="-Wl,-rpath -Wl,$x_ac_cv_metricq_dir/$bit -L$x_ac_cv_metricq_dir/$bit"
      else
        METRICQ_LDFLAGS="-L$x_ac_cv_metricq_dir/$bit"
      fi
      METRICQ_LIBS="-lmetricq-history"
    fi

    AC_SUBST(METRICQ_LIBS)
    AC_SUBST(METRICQ_CXXFLAGS)
    AC_SUBST(METRICQ_LDFLAGS)
  fi
  AC_LANG_POP([C++])

  AM_CONDITIONAL(BUILD_METRICQ, test -n "$x_ac_cv_metricq_dir")
])
