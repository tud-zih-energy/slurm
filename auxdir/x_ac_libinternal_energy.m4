##*****************************************************************************
#  AUTHOR:
#    Written by Bull- Thomas Cadeau
#
#  SYNOPSIS:
#    X_AC_LIBINTERNAL_ENERGY
#
#  DESCRIPTION:
#    Determine if the LIBINTERNAL_ENERGY libraries exists
##*****************************************************************************

AC_DEFUN([X_AC_LIBINTERNAL_ENERGY],
[
  _x_ac_libinternal_energy_dirs="/usr /usr/local"
  _x_ac_libinternal_energy_libs="lib64 lib"
  AC_LANG_PUSH([C++])
  AC_ARG_WITH(
    [libinternal_energy],
    AS_HELP_STRING(--with-libinternal_energy=PATH,Specify path to libinternal_energy-devel installation),
    [AS_IF([test "x$with_libinternal_energy" != xno && test "x$with_libinternal_energy" != xyes],
           [_x_ac_libinternal_energy_dirs="$with_libinternal_energy"])])

  if [test "x$with_libinternal_energy" = xno]; then
    AC_MSG_WARN([support for libinternal_energy disabled])
  else
    AC_CACHE_CHECK(
      [for libinternal_energy installation],
      [x_ac_cv__dir],
      [
        for d in $_x_ac_libinternal_energy_dirs; do
          test -d "$d" || continue
          test -d "$d/include" || continue
          test -d "$d/include/internal_energy" || continue
          test -f "$d/include/internal_energy/energy.hpp" || continue
          for bit in $_x_ac_libinternal_energy_libs; do
            test -d "$d/$bit" || continue
            _x_ac_libinternal_energy_cxxflags_save="$CXXFLAGS"
            CXXFLAGS="-I$d/include $CXXFLAGS"
            _x_ac_libinternal_energy_libs_save="$LIBS"
            LIBS="-L$d/$bit -linternal_energy -lperf-cpp $LIBS"
            AC_LINK_IFELSE(
              [AC_LANG_PROGRAM(
                 [[
                   #include <internal_energy/energy.hpp>
                 ]],
                 [[
			auto counters = internal_energy::get_energy_counters();
			
                 ]],
               )],
              [AS_VAR_SET(x_ac_cv_libinternal_energy_dir, $d)],
              [])
            CXXFLAGS="$_x_ac_libinternal_energy_cxxflags_save"
            LIBS="$_x_ac_libinternal_energy_libs_save"
            test -n "$x_ac_cv_libinternal_energy_dir" && break
          done
          test -n "$x_ac_cv_libinternal_energy_dir" && break
        done
      ])

    # echo x_ac_cv_libinternal_energy_dir $x_ac_cv_libinternal_energy_dir
    if test -z "$x_ac_cv_libinternal_energy_dir"; then
      if test -z "$with_libinternal_energy"; then
        AC_MSG_WARN([unable to locate libinternal_energy installation])
      else
        AC_MSG_ERROR([unable to locate libinternal_energy installation])
      fi
    else
      LIBINTERNAL_ENERGY_CXXFLAGS="-I$x_ac_cv_libinternal_energy_dir/include"
      if test "$ac_with_rpath" = "yes"; then
        LIBINTERNAL_ENERGY_LDFLAGS="-Wl,-rpath -Wl,$x_ac_cv_libinternal_energy_dir/$bit -L$x_ac_cv_libinternal_energy_dir/$bit"
      else
        LIBINTERNAL_ENERGY_LDFLAGS="-L$x_ac_cv_libinternal_energy_dir/$bit"
      fi
      LIBINTERNAL_ENERGY_LIBS="-linternal_energy -lperf-cpp"
    fi

    AC_SUBST(LIBINTERNAL_ENERGY_LIBS)
    AC_SUBST(LIBINTERNAL_ENERGY_CXXFLAGS)
    AC_SUBST(LIBINTERNAL_ENERGY_LDFLAGS)
  fi
  AC_LANG_POP([C++])

  AM_CONDITIONAL(BUILD_LIBINTERNAL_ENERGY, test -n "$x_ac_cv_libinternal_energy_dir")
])
